# Sniffit
Sniffit allows you to sniff your LAN traffic using Wireshark expressions and printing out the captured payload. You can think of Sniffit as a modest Wireshark.

## Building
For building, just run 'make'.

Note that Sniffit is based on [libpcap](http://www.tcpdump.org/), so you will need to install this dependency in order to run it.

### Other
The 'solution' folder contains the files to manage the project and make its development easier, but it depends on a graphical enviroment.
Currently it is handle by QtCreator.


------------

Best regards

Ernesto Messina
