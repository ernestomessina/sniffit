###############################
# Ernesto Messina
# 2017-03-09
###############################

GXX = g++
FLAGS = -std=c++14 -Wall
LIBS = -lpcap -lsnif -llogit -pthread
LIB_PATH = -Llib
LD_LIB_PATH = lib/
DEBUG = -g
OUTPUTDIR = bin
APPNAME = sniffit
# Reads the working directory name
VERSION = $(notdir $(shell pwd))
SRCDIR = src/
SOURCESDIR = src/sources


all: create_dir main.o capture_man.o opt_reader.o create_launcher
	$(GXX) $(FLAGS) $(OUTPUTDIR)/*.o -o $(OUTPUTDIR)/$(APPNAME) $(LIBS) $(LIB_PATH)

debug: FLAGS += $(DEBUG)
debug: all

strip_symbols:
	strip -s $(OUTPUTDIR)/*

main.o: $(SOURCESDIR)/main.cpp
	$(GXX) $(FLAGS) -c $(SOURCESDIR)/main.cpp -o $(OUTPUTDIR)/main.o

capture_man.o: $(SOURCESDIR)/capture_man.cpp
	$(GXX) $(FLAGS) -c $(SOURCESDIR)/capture_man.cpp -o $(OUTPUTDIR)/capture_man.o

opt_reader.o: $(SOURCESDIR)/opt_reader.cpp
	$(GXX) $(FLAGS) -c $(SOURCESDIR)/opt_reader.cpp -o $(OUTPUTDIR)/opt_reader.o

create_dir:
	mkdir -p $(OUTPUTDIR)

dist: all remove_object_files strip_symbols create_pkg clean

create_pkg:
	tar cfJ $(APPNAME)-$(VERSION).tar.xz $(OUTPUTDIR) $(LD_LIB_PATH)

create_launcher:
	cp $(SRCDIR)/launcher.sh .
	chmod +x launcher.sh

remove_object_files:
	rm -f $(OUTPUTDIR)/*.o

clean:
	rm -rf $(OUTPUTDIR) *~ launcher.sh

help:
	@echo "The following are some of the valid targets:"
	@echo "... all (the default if no target is provided)"
	@echo "... debug"
	@echo "... remove_object_files"
	@echo "... clean"
	@echo "... dist (creates a distribution package)"
