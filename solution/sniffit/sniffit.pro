TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lpcap -lsnif -llogit -lpthread

SOURCES += \
    ../../src/sources/main.cpp \
    ../../src/sources/capture_man.cpp \
    ../../src/sources/opt_reader.cpp

HEADERS += \
    opt_reader.h \
    ../../src/headers/opt_reader.h \
    ../../src/headers/capture_man.h \
    ../../src/headers/color_output.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../lib/release/ -lsnif
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../lib/debug/ -lsnif
else:unix:!macx: LIBS += -L$$PWD/../../lib/ -lsnif

INCLUDEPATH += $$PWD/../../
DEPENDPATH += $$PWD/../../
