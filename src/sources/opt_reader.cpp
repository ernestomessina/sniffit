#include "../../src/headers/opt_reader.h"
#include <unistd.h>	// getopt()

auto opt_reader::read_opt(int argc, char *const *argv, const char *options) -> void
{
    int c {};
    while ((c = getopt(argc, argv, options)) != -1)
    {
        switch (c)
        {
            case 'd':
            {
                optmap.emplace(std::make_pair(opt_reader::options::device, optarg));
                break;
            }
            case 'e':
            {
                optmap.emplace(std::make_pair(opt_reader::options::expression, optarg));
                break;
            }
            case 'f':
            {
                optmap.emplace(std::make_pair(opt_reader::options::filename, optarg));
                break;
            }
            case ':':	// Option requires and argument
            case '?':	// Invalid option
            {
                optmap.emplace(std::make_pair(opt_reader::options::invalid, optarg));
            }
        }
    }
}

const std::unordered_map<int, std::string>& opt_reader::get_optmap() const
{
    return optmap;
}
