#include <iostream>
#include <string>
#include <string.h>             // strcmp()
#include <chrono>               // chrono::seconds
#include <iomanip>              // For printing: setw(), setfill()
#include <arpa/inet.h>          // inet_ntoa()
#include <net/ethernet.h>
#include <netinet/ip.h>         // Declarations for IP header
#include <netinet/tcp.h>        // Declarations for TCP header
#include <netinet/udp.h>        // Declarations for UDP header
#include <netinet/igmp.h>       // Declarations for IGMP header
#include <netinet/ip_icmp.h>    // Declarations for ICMP header
#include "../headers/capture_man.h"

namespace sniffit
{

    capture_man::capture_man(int argc, char **argv)
        : expression { "port 80" }
    {
        setup_sniffer(argc, argv);
    }

    capture_man::~capture_man()
    {
        if (capturing_thread.joinable())
            capturing_thread.join();
        if (cap_man && capturing)
            cap_man->stop_capture();
    }

    void capture_man::start_capture()
    {
        file_log << tcolor::tc_red << "Test color loging to file" << tcolor::tc_reset << endl;
        file_log << "Test loging to file" << endl;

        if (device.empty())
        {
            std::string opt {};
            std_log << "No device selected. Will perform device lookup." << endl;
            list_devices();
            auto list = cap_man->get_list_dev();
            std_log << "Enter a device: "; std_log.flush();
            std::getline(std::cin, opt);
            set_capture_device(list.at(static_cast<size_t>(std::atoi(opt.c_str())) - 1).name);
        }

        std_log << "To stop capturing press 'q'. Start capture now? [Y/n]: "; std_log.flush();
        std::string opt { "Y" };
        std::getline(std::cin, opt);

        if (opt.empty() || opt == "Y" || opt == "y")
        {
            if (capturing_thread.joinable())
                capturing_thread.join();
            capturing_thread = std::thread(&capture_man::_start_capture, this);
        }
        else
            exit(EXIT_SUCCESS);

        opt.clear();
        while (opt != "q")
        {
            std::cin >> opt;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        if (cap_man)
        {
            std_log << "(some traffic may need arrive to get stopped)" << endl;
            std_log << "Stopping..."; std_log.flush();
            cap_man->stop_capture();
            capturing_thread.join();
            std_log << "\tDONE" << endl;
        }
    }

    void capture_man::_start_capture()
    {
        if (!cap_man) return;

        expression = cap_man->get_expression();
        if (filename.empty())
            device = cap_man->get_device();
        else
            device = filename;

        std_log << "Sniffing: " << tcolor::tc_green << expression << tcolor::tc_reset << endl;
        std_log << "Using device: " << tcolor::tc_green << device << tcolor::tc_reset << endl << endl;

        try
        {
            if (!filename.empty())
                cap_man->capture_from_file(filename);

            capturing = true;
            std_log << "Capturing..." << endl;
            cap_man->start_capture();
            capturing = false;
        }
        catch(pcap_excep_device& e)
        {
            err_log << "Error setting device. Try elevation. (" << e.what() << ")" << endl;
            err_log << "Error: " << cap_man->get_error() << endl;
            err_log << "Exception message: " << cap_man->get_excep_msg() << endl;
        }
        catch(pcap_excep_net& e)
        {
            err_log << "Network exception: " << e.what() << endl;
            err_log << "Error: " << cap_man->get_error() << endl;
            err_log << "Exception message: " << cap_man->get_excep_msg() << endl;
        }

        catch(pcap_excep_filter& e)
        {
            err_log << "Filter exception: " << e.what() << endl;
            err_log << "Error: " << cap_man->get_error() << endl;
            err_log << "Exception message: " << cap_man->get_excep_msg() << endl;
        }
        catch(pcap_excep& e)
        {
            err_log << "Generic exception: " << e.what() << endl;
            err_log << "Error: " << cap_man->get_error() << endl;
            err_log << "Exception message: " << cap_man->get_excep_msg() << endl;
        }
        catch (std::exception& e)
        {
            err_log << "Not a Sniffit exception. Bad device?: " << e.what() << endl;
            err_log << "Error: " << cap_man->get_error() << endl;
            err_log << "Exception message: " << cap_man->get_excep_msg() << endl;
        }
    }

    void capture_man::stop_capture()
    {
        if (cap_man) cap_man->stop_capture();
    }

    void capture_man::show_usage(const char *argv0)
    {
        std_log << "Sniffit by Ernesto Messina" << endl;
        std_log << "Usage: " << argv0 << ": [-d <interface>] [-e <expression>]" << endl;
    }

    void capture_man::list_devices()
    {
        auto list = cap_man->get_list_dev();
        size_t j { 1 };
        for (size_t i = 0; i < list.size(); ++i, ++j)
        {
            if (list.at(i).flags)
                std_log << j << ". " << list.at(i).name << endl;
        }
    }

    void capture_man::setup_sniffer(int argc, char **argv)
    {
        if (argc == 2 && strcmp(argv[1], "--help") == 0)
        {
            show_usage(argv[0]);
            exit(EXIT_FAILURE);
        }

        std_log.set_time_stamp_enable(false);
        err_log.set_time_stamp_enable(false);

        load_parameter(argc, argv);

        cap_man = std::make_unique<pcap_man>(expression, capture_man::got_package_cb, device);
    }

    void capture_man::set_capture_device(const std::string &dev)
    {
        if (cap_man) cap_man->set_device(dev);
    }

    auto capture_man::load_parameter(int argc, char **argv) -> void
    {
        param_reader.read_opt(argc, argv, "d:e:f:");
        if (param_reader.get_optmap().empty())
        {
            std_log << tcolor::tc_red << "No option passed. Setting up defaults." << tcolor::tc_reset << endl;
            return;
        }

        for (std::unordered_map<int, std::string>::const_iterator it = begin(param_reader.get_optmap());
             it != end(param_reader.get_optmap()); ++it)
        {
            switch (it->first)
            {
                case opt_reader::options::expression:
                    expression = it->second;
                    break;
                case opt_reader::options::filename:
                    filename = it->second;
                    break;
                case opt_reader::options::device:
                    device = it->second;
                    break;
                case opt_reader::options::invalid:
                    //std_log << tc_red << "Invalid option passed. Setting up defaults." << tc_reset << std::endl;
                    std_log << tcolor::tc_red << "Invalid option passed. Setting up defaults." << tcolor::tc_reset << endl;
                    break;
                default:
                    show_usage(argv[0]);
            }
        }
    }

    void capture_man::print_payload(const u_char *payload, int len) // static
    {
        if (!payload) return;

        const u_char *ch { payload };
        long offset {}, index {};

        while (index < len)
        {
            // Print offset
            std::cout << std::dec << std::setfill('0') << std::setw(4) << offset << "   ";
            offset += 16;

            // Print first 8 bytes in hex
            for (int j = 0; j < 8 && index < len; ++j)
            {
                std::cout << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(ch[index]) << " ";
                ++index;
            }

            std::cout << "  ";

            // Print second 8 bytes in hex
            for (int j = 0; j < 8 && index < len; ++j)
            {
                std::cout << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(ch[index]) << " ";
                ++index;
            }

            // Print ASCII
            std::cout << "    ";
            for (int k = 16; k > 0; --k)
            {
                if (isprint(ch[index - k]))
                    std::cout << ch[index - k];
                else
                    std::cout << ".";
            }

            std::cout << std::endl;
        }
    }

    void capture_man::got_package_cb(u_char *args, const pcap_pkthdr *header, const u_char *packet) // static
    {
        (void)args; (void)header;
        //int size = header->len;
        static int pkg_count {};
        int size_payload {};
        const unsigned char *payload {};
        struct sockaddr_in source, dest;
        memset(&source, 0, sizeof(source));
        memset(&dest, 0, sizeof(dest));

        pkg_count++;
        std::cout << std::endl << "Packet number: " << std::dec << pkg_count << std::endl;

        // Read IP header
        struct iphdr *ip_hdr = (struct iphdr*)(packet + sizeof(struct ethhdr));
        int ip_hdr_len { ip_hdr->ihl * 4 };

        // Print source and destination IP addresses
        source.sin_addr.s_addr = ip_hdr->saddr;
        dest.sin_addr.s_addr = ip_hdr->daddr;
        std::cout << "       From: " << inet_ntoa(source.sin_addr) << std::endl;
        std::cout << "         To: " << inet_ntoa(dest.sin_addr) << std::endl;

        ////////////////////////////////////////////
        // Determine protocol
        switch (ip_hdr->protocol)
        {
            case ICMP_Protocol:
            {
                std::cout << "Protocol: ICMP" << std::endl;

                struct icmphdr *icmp_hdr = (struct icmphdr *)(packet + ip_hdr_len + sizeof(struct ethhdr));
                int icmp_hdr_size = sizeof(struct ethhdr) + ip_hdr_len + sizeof(icmp_hdr);

                if (static_cast<uint>(icmp_hdr->type) == 11)
                    std::cout << "(TTL Expired)" << std::endl;
                else
                    std::cout << "(ICMP echo reply)" << std::endl;
                
                // Payload (segment) offset
                payload = (u_char *)(packet + sizeof(struct ethhdr) + ip_hdr_len + icmp_hdr_size);

                // Payload (segment) size
                size_payload = ntohs(ip_hdr->tot_len) - (ip_hdr_len + icmp_hdr_size);
            }
            break;
            case IGMP_Protocol:
            {
                std::cout << "Protocol: IGMP" << std::endl;

                struct igmp *igmp_hdr = (struct igmp *)(packet + ip_hdr_len + sizeof(struct ethhdr));
                int igmp_hdr_size = sizeof(struct ethhdr) + ip_hdr_len + sizeof(igmp_hdr);

                // Payload (segment) offset
                payload = (u_char *)(packet + sizeof(struct ethhdr) + ip_hdr_len + igmp_hdr_size);

                // Payload (segment) size
                size_payload = ntohs(ip_hdr->tot_len) - (ip_hdr_len + igmp_hdr_size);
            }
            break;
            case TCP_Protocol:
            {
                struct tcphdr *tcp_hdr = (struct tcphdr*)(packet + ip_hdr_len + sizeof(struct ethhdr));
                int tcp_hdr_size = tcp_hdr->doff * 4;

                std::cout << "Src port: " << ntohs(tcp_hdr->th_sport) << std::endl;
                std::cout << "Dst port: " << ntohs(tcp_hdr->th_dport) << std::endl;

                // Payload (segment) offset
                payload = (u_char *)(packet + sizeof(struct ethhdr) + ip_hdr_len + tcp_hdr_size);

                // Payload (segment) size
                size_payload = ntohs(ip_hdr->tot_len) - (ip_hdr_len + tcp_hdr_size);
            }
            break;
            case UDP_Protocol:
            {
                std::cout << "Protocol: UDP" << std::endl;

                struct udphdr *udp_hdr = (struct udphdr*)(packet + ip_hdr_len + sizeof(struct ethhdr));
                int udp_hdr_size = sizeof(struct ethhdr) + ip_hdr_len + sizeof(udp_hdr);

                std::cout << "Src port: " << ntohs(udp_hdr->uh_sport) << std::endl;
                std::cout << "Dst port: " << ntohs(udp_hdr->uh_dport) << std::endl;

                // Payload (segment) offset
                payload = (u_char *)(packet + sizeof(struct ethhdr) + ip_hdr_len + udp_hdr_size);

                // Payload (segment) size
                size_payload = ntohs(ip_hdr->tot_len) - (ip_hdr_len + udp_hdr_size);
            }
            break;
            default:
                std::cout << "Protocol: other" << std::endl;
                std::cout << "Won't print the payload." << std::endl;
            break;
        }

        // Print payload data
        if (size_payload > 0)
        {
            std::cout << "   Payload (" << size_payload << " bytes)"<< std::endl;
            print_payload(payload, size_payload);
        }
    }

}   // sniffit namespace
