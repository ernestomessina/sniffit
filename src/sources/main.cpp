/***
 * Network traffic sniffer.
 *
 * @Name: Sniffit
 * @Date: 2017-03-05
 * @Author: Ernesto Messina
*/
#include "../../src/headers/capture_man.h"
using namespace sniffit;


int main(int argc, char *argv[])
{
    capture_man capturer(argc, argv);
    capturer.start_capture();

    return EXIT_SUCCESS;
}
