#ifndef COLOR_OUTPUT_H
#define COLOR_OUTPUT_H

#include <string>
#include <unistd.h> // isatty()
#include <exception>

namespace tcolor
{

    // Color definitions
    const std::string tc_reset("\033[0m");
    const std::string tc_red("\033[31m");
    const std::string tc_green("\033[32m");
    const std::string tc_yellow("\033[33m");
    const std::string tc_blue("\033[34m");
    const std::string tc_magenta("\033[35m");
    const std::string tc_cyan("\033[36m");
    const std::string tc_white("\033[37m");
    const std::string tc_bold_back("\033[1m\033[30m");
    const std::string tc_bold_red("\033[1m\033[31m");
    const std::string tc_bold_green("\033[1m\033[32m");
    const std::string tc_bold_yellow("\033[1m\033[33m");
    const std::string tc_bold_blue("\033[1m\033[34m");
    const std::string tc_bold_magenta("\033[1m\033[35m");
    const std::string tc_bold_cyan("\033[1m\033[36m");
    const std::string tc_bold_white("\033[1m\033[37m");


    // Compatibility checks
    class color_output_excep : public std::exception
    {
    public:
        virtual const char* what() const throw() { return "The ouput device does not support colors."; }
    };

    struct _color_output_helpers
    {
        bool supports_colors()
        {
            if (isatty(0) && isatty(1))
                return true;

            color_output_excep excep;
            throw excep;
        }
    };
    
    // Example of use
    // std::cout << tc_red << "Hello world!" << tc_reset << std::endl;
    

}   // tcolor namespace

#endif // COLOR_OUTPUT_H
