/***
 * Logger base class.
 *
 * @Date: 2017-01-01
 * @Author: Ernesto Messina
**/
#ifndef LOGGER_BASE_H
#define LOGGER_BASE_H

#include "logger.h"
#include <chrono>
#include <sstream>
#include "color_output.h"

namespace logit
{
    using namespace tcolor;

    class logger_base : public logger
    {
    public:
        logger_base(const log_type log_type, const log_level log_level);
        virtual ~logger_base();

        // logger implementation
        virtual log_level get_level() const override;
        virtual log_type get_type() const override;
        void set_level(const log_level new_level) override;
        void set_time_stamp_enable(const bool enable = true);
        virtual void flush() override = 0;
        virtual bool support_colors() const noexcept(false) override;

        logger_base& operator<<(const std::string& msg);
        logger_base& operator<<(const int msg);

    protected:
        std::string get_time_stamp() const;
        std::string get_milliseconds_part() const;
        void trim_decimals(std::string *str) const;
        void normalize_decimals(std::string *str) const;
        log_level detect_buffer_log_level() const;
        void remove_lvl_text();
        void filter_out_msg_by_lvl();

    protected:
        log_type type;
        log_level level;
        std::chrono::time_point<std::chrono::high_resolution_clock> first_time;
        bool print_time_stamp { true };
        bool time_stamp_enable { true };
        std::stringstream buffer;
    };

}   // logit

#endif // LOGGER_BASE_H
