/***
 * Option environment reader.
 *
 * @Name: libsnif
 * @Date: 2017-03-15
 * @Author: Ernesto Messina
*/
#ifndef OPT_READER_H
#define OPT_READER_H

#include <unordered_map>

class opt_reader
{
public:
    enum /*class */options
    {
        device      = 0x2,
        expression	= 0x4,
        filename    = 0x8,
        none        = 0x10,
        invalid		= 0x20
    };

    /**
     * @brief Calls getopt()
     * @param argc argc from main.
     * @param argv argv from main.
     * @param options Options to read.
     * @return An OPTIONS bitmask.
     */
    auto read_opt(int argc, char *const *argv, const char *options) -> void;

    const std::unordered_map<int, std::string>& get_optmap() const;

private:
    std::unordered_map<int, std::string> optmap;
};

#endif // OPT_READER_H
