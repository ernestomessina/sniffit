/***
 * Logger interface.
 *
 * @Date: 2017-01-01
 * @Author: Ernesto Messina
**/
#ifndef LOGGER_H
#define LOGGER_H

#include <string>

namespace logit
{

    enum class log_type
    {
        standar,    // Standar output
        error,      // Standar error
        file        // File
    };

    enum class log_level
    {
        invalid,    // Invalid log level
        info,       // Information level
        warn,       // Warning level
        error,      // Error level
        debug       // Debug level
    };

    //////////////////////////////
    // Keywords
    const std::string endl { "\n" };
    const std::string info  { "_info_" };
    const std::string warn  { "_warn_" };
    const std::string debug { "_dbug_" };

    //////////////////////////////
    // Interface for all logger types.
    class logger
    {
    public:
        virtual ~logger() {}

        /**
         * @brief Return the used logging level mechanism.
         * @return A log_level representing the used logging level.
         */
        virtual log_level get_level() const = 0;

        /**
         * @brief Return the used logging type mechanism.
         * @return A log_type representing the used mechanism.
         */
        virtual log_type get_type() const = 0;

        /**
         * @brief Set a new log level.
         * @param new_level The new log level.
         */
        virtual void set_level(const log_level new_level) = 0;

        /**
         * @brief Flush the buffer.
         */
        virtual void flush() = 0;

        /**
         * @brief Tell if the output device supports colors.
         * @return Return true if the device support colors, and false otherwise.
         */
        virtual bool support_colors() const noexcept(false) = 0;
    };

}   // logit

#endif // LOGGER_H
