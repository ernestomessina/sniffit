/***
 * Standar logger class.
 *
 * @Date: 2017-01-01
 * @Author: Ernesto Messina
**/
#ifndef STD_LOGGER_H
#define STD_LOGGER_H

#include "logger_base.h"
#include <iostream>

namespace logit
{

    class std_logger : public logger_base
    {
    public:
        explicit std_logger(const log_level log_level = log_level::info);
        virtual ~std_logger();

        // logger_base implementation
        virtual void flush();
    };

}   // logit

#endif // STD_LOGGER_H
