/***
 * File logger class.
 *
 * @Date: 2017-01-01
 * @Author: Ernesto Messina
**/
#ifndef FILE_LOGGER_H
#define FILE_LOGGER_H

#include "logger_base.h"
#include <fstream>

namespace logit
{

    ////////////////////////////
    // Size definitions
    const uint kilobyte = 1024;
    const uint megabyte = 1048576;
    const uint gigabyte = 1073741824;

    ////////////////////////////
    // file_logger class
    class file_logger : public logger_base
    {
    public:
        explicit file_logger(const std::string& file_path, const size_t max_file_size = megabyte, const log_level log_level = log_level::info);
        virtual ~file_logger();

        /**
         * @brief Return the current log size.
         * @return A size_t containing the current log size.
         */
        size_t get_log_size() const;

        /**
         * @brief Return the maximum log size allows.
         * @return A size_t containing the maximum current log size.
         */
        size_t get_log_max_size() const;

        /**
         * @brief Set a new maximum log size.
         * @param new_max_size A uint with the new max log size.
         */
        void set_log_max_size(const uint new_max_size) noexcept;

        // logger_base implementation
        virtual void flush();

    protected:
        std::string find_path_slot();
        bool create_new_log() noexcept(false);
        bool file_exists(const std::string& path) const;
        void remove_ext(std::string& str);

    protected:
        std::ofstream file;
        std::string path;
        size_t max_size;
    };

}   // logit

#endif // FILE_LOGGER_H
