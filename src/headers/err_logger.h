/***
 * Error logger class.
 *
 * @Date: 2017-01-01
 * @Author: Ernesto Messina
**/
#ifndef ERR_LOGGER_H
#define ERR_LOGGER_H

#include "logger_base.h"
#include <iostream>

namespace logit
{

    class err_logger : public logger_base
    {
    public:
        explicit err_logger(const log_level log_level = log_level::info);
        virtual ~err_logger();

        // logger_base implementation
        virtual void flush();
    };

}   // logit

#endif // ERR_LOGGER_H
