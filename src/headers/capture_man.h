/***
 * Sniffit.
 *
 * @Name: Sniffit
 * @Date: 2017-03-15
 * @Author: Ernesto Messina
*/
#ifndef CAPTURE_MAN_H
#define CAPTURE_MAN_H

#include "../headers/pcap_man.h"
#include "../headers/opt_reader.h"
#include "../headers/logit.h"
#include <thread>
using namespace logit;

namespace sniffit
{
    // Definitions
    const int ICMP_Protocol { 1 };
    const int IGMP_Protocol { 2 };
    const int TCP_Protocol  { 6 };
    const int UDP_Protocol  { 17 };


    // capture-Man class
    class capture_man
    {
    public:
        /**
         * @brief Constructor.
         * @param argc main's argc.
         * @param argv main's argv.
         */
        capture_man(int argc, char **argv);
        ~capture_man();

        /**
         * @brief Start sniffing.
         * @return Return 0 on success, -1 on error.
         */
        void start_capture();

        /**
         * @brief Stop sniffing.
         */
        void stop_capture();

        /**
         * @brief Print the device list found.
         */
        void list_devices();

    private:
        void _start_capture();
        void show_usage(const char *argv0);
        void setup_sniffer(int argc, char **argv);
        void set_capture_device(const std::string& dev);
        auto load_parameter(int argc, char **argv) -> void;

         // Print data in rows of 16 bytes: offset   hex   ascii
         // 00000   47 45 54 20 2f 20 48 54  54 50 2f 31 2e 31 0d 0a   GET / HTTP/1.1..
        static void print_payload(const u_char *payload, int len);

        // Callback on package arrive
        static void got_package_cb(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);

    private:
        std::unique_ptr<pcap_man> cap_man;
        opt_reader param_reader;
        std::string expression;
        std::string device;
        std::string filename;
        std::thread capturing_thread;
        bool using_colors { true };
        bool capturing { false };
        std_logger std_log;
        err_logger err_log;
        file_logger file_log { "bin/log.txt", megabyte };
    };

}   // sniffit namespace

#endif // CAPTURE_MAN_H
