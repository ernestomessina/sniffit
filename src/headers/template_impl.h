/***
 * Template implementations file.
 *
 * @Date: 2017-04-09
 * @Author: Ernesto Messina
**/
#ifndef TEMPLATE_IMPL_H
#define TEMPLATE_IMPL_H

#include "std_logger.h"
#include "err_logger.h"
#include "file_logger.h"

namespace logger_space
{

    template<typename T>
    void process_endl(logger_base& log, const T& type)
    {
        log.show_time_stamp = true;
        log.buffer << type;

        auto msg_level_detected = log.detect_buffer_log_level();
        if (log_level::debug == log.level ||
                log.level == msg_level_detected ||
                msg_level_detected == log_level::invalid)
        {
            log.remove_lvl_text();
            log.flush();
        }
        else
            log.buffer.str("");
    }

    template<typename T>
    void process_msg(logger_base& log, const T& type)
    {
        log.show_time(log);

        if (type == endl)
        {
            process_endl(log, type);
            return;
        }

        log.buffer << type;
    }

    inline bool operator==(const int n, const std::string& str)
    {
        std::string sn { std::to_string(n) };
        return str == sn;
    }

    //////////////////////////////////
    /// std_logger template.
    template<typename T>
    std_logger& operator<<(std_logger& log, const T& type)
    {
        process_msg(log, type);
        return log;
    }

    //////////////////////////////////
    /// err_logger template.
    template<typename T>
    err_logger& operator<<(err_logger& log, const T& type)
    {
        process_msg(log, type);
        return log;
    }

    //////////////////////////////////
    /// file_logger template.
    template<typename T>
    auto& operator<<(file_logger& log, const T& type)
    {
        process_msg(log, type);
        return log;
    }

}   // logger_space

#endif // TEMPLATE_IMPL_H
